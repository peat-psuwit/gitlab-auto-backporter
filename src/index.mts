#!/usr/bin/env node

// SPDX-License-Identifier: ISC
/*
 * Copyright (C) 2024 UBports Foundation.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

import { fork } from 'node:child_process';
import fs from 'node:fs';

import { Command, Option } from '@commander-js/extra-typings';
import { sync as resolveBinSync } from 'resolve-bin';

import { MergeRequestEvent } from './types/gitlab.mjs';
import { Args as GitBackportingArgs } from './types/git-backporting.mjs';

function mrSlug(body: MergeRequestEvent) {
    return `${body.project.path_with_namespace}!${body.object_attributes.iid}`;
}

function calculateBackportTargets(body: MergeRequestEvent): string[] | null {
    let labels: string[];
    let bp_targets: string[] = [];

    body.object_attributes.url;

    switch (body.object_attributes.action) {
        case "merge":
            labels = body.labels.map(l => l.title);
            break;
        case "update":
            if (body.object_attributes.state !== 'merged') {
                console.log(`${mrSlug(body)}: ignore changes in unmerged MR.`);
                return null;
            }

            const { labels: changedLabels } = body.changes;
            if (!changedLabels) {
                console.log(`${mrSlug(body)}: ignore changes other than labels.`);
                return null;
            }

            const previousLabels =changedLabels.previous.map(l => l.title);
            const currentLabels = changedLabels.current.map(l => l.title);

            labels = currentLabels.filter(l => !previousLabels.includes(l));
            break;
        default:
            console.log(`${mrSlug(body)}: ignore actions other than 'merge' and 'update'.`);
            return null;
    }

    for (let label of labels) {
        const prefix = 'Backport to: ';
        if (!label.startsWith(prefix))
            continue;

        let bp_target = label.substring(prefix.length);
        bp_targets.push(bp_target);
    }

    if (bp_targets.length == 0) {
        console.log(`${mrSlug(webhook_payload)}: no backport targets, do nothing.`);
        return null;
    }

    return bp_targets;
}

async function runGitBackporting(configFile: string, gitlabApiToken?: string) {
    let env = process.env;
    if (gitlabApiToken) {
        process.env['GITLAB_TOKEN'] = gitlabApiToken;
    }

    let gitBackportingPath = resolveBinSync(
        '@kie/git-backporting', { executable: 'git-backporting' });

    let gitBackportingProcess = fork(gitBackportingPath,
        ['--config-file', configFile], { env: env, stdio: 'pipe' });

    let output = '';

    gitBackportingProcess.stdout?.on('data', (data: Buffer) => {
        process.stdout.write(data);
        output += data.toString('utf-8');;
    });

    gitBackportingProcess.stderr?.on('data', (data: Buffer) => {
        process.stderr.write(data);
        output += data.toString('utf-8');
    });

    let exitCode = await new Promise<number>(
            (resolv) => gitBackportingProcess.on('close', resolv));

    return {
        exitCode, output,
    };
}

const program = new Command()
    .addOption(new Option(
        "--gitlab-api-token <token>",
        "A GitLab access token - OAuth2, personal, project, group. Required unless --dry-run is specified.")
        .env("GITLAB_API_TOKEN"))
    .addOption(new Option("--trigger-payload <json file>", "Path of the file with webhook result.")
        .env("TRIGGER_PAYLOAD")
        .makeOptionMandatory(true))
    .addOption(new Option("--dry-run", "Don't actually create MR on GitLab.")
        .env("GITLAB_AUTO_BACKPORTER_DRYRUN"))
    .addOption(new Option("--git-user <name>", "Name used as Git committer.")
        .env("GITLAB_AUTO_BACKPORTER_GITUSER")
        .default("UBports backport bot"))
    .addOption(new Option("--git-email <email>", "E-mail used as Git committer.")
        .env("GITLAB_AUTO_BACKPORTER_GITEMAIL")
        .default("dev@ubports.com"))
    .addOption(new Option("--keep-workdir", "Keep workdir for debugging.")
        .env("GITLAB_AUTO_BACKPORTER_KEEP_WORKDIR"))
    .showHelpAfterError()
    .parse();

const opts = program.opts()
// Hmm... maybe there's a way to do this declaratively?
if (!opts.gitlabApiToken && !opts.dryRun) {
    console.error("error: GitLab API token is not provided and --dry-run is not specified.");
    console.error("");
    program.help();
    process.exit(1);
}

const webhook_payload: MergeRequestEvent = JSON.parse(
    fs.readFileSync(opts.triggerPayload, { encoding: 'utf-8' }));
if (webhook_payload.object_kind !== 'merge_request') {
    console.log('error: received webhook is not for a merge request.');
    process.exit(1);
}

let bp_targets = calculateBackportTargets(webhook_payload);
if (!bp_targets) {
    process.exit(0);
}

let tempDir = fs.mkdtempSync('gitlab-auto-backporter-');
process.on('exit', () => {
    if (opts.keepWorkdir) {
        console.log(`Not cleaning up (as requested). Workdir is at ${tempDir}`);
    } else {
        console.log('Cleaning up.');
        fs.rmSync(tempDir, { recursive: true, force: true });
    }
});

console.log(`Backporting ${mrSlug(webhook_payload)} ` +
            `to the following branches: ${bp_targets.join(', ')}`);

let gitBackportingArgs: GitBackportingArgs = {
    // Note: targetBranch is a comma-separated string
    targetBranch: bp_targets.join(','),
    pullRequest: webhook_payload.object_attributes.url,
    dryRun: opts.dryRun,
    folder: `${tempDir}/bp`,
    gitUser: opts.gitUser,
    gitEmail: opts.gitEmail,
    // Note: bpBranchName is a comma-separated string
    bpBranchName: bp_targets
        .map(target => `backports/mr-${webhook_payload.object_attributes.iid}/${target}`)
        .join(','),
    autoNoSquash: true,
    strategyOption: "", // Override default of "theirs"
    cherryPickOptions: "-x", // Add "(cherry picked from commit ...)" trailer
};

let configFile = `${tempDir}/config.json`;
fs.writeFileSync(configFile, JSON.stringify(gitBackportingArgs));
let result = await runGitBackporting(configFile, opts.gitlabApiToken);

console.log(`git-backporting exited with status ${result.exitCode}.`);

if (opts.dryRun) {
    process.exit(0);
}

/* Enable "Delete source branch when merge request accepted". We do this by
 * fishing out MR number from git-backporting's output. This is somewhat
 * fragile, but since it's not that important anyway, :shrug:.
 *
 * This should be built-in within git-backporting, but it's not at the moment.
 * https://github.com/kiegroup/git-backporting/issues/126
 */

const prCreatedRegex = new RegExp(
    "Pull request created: https://gitlab.com/.*/-/merge_requests/([0-9]+)", "g");
let prCreatedRegexMatched = false;
let match: RegExpExecArray | null;
while (match = prCreatedRegex.exec(result.output)) {
    let createdMrIid = match[1];
    let deleteSourceResponse = await fetch(`https://gitlab.com/api/v4/projects/` +
            encodeURIComponent(webhook_payload.project.path_with_namespace) +
            `/merge_requests/${createdMrIid}`,
        {
            method: 'PUT',
            headers: { 
            "Authorization": `Bearer ${opts.gitlabApiToken}`,
            "Content-Type": "application/json",
            },
            body: JSON.stringify({
                remove_source_branch: true,
            }),
        });

    if (!deleteSourceResponse.ok) {
        console.warn(`WARNING: setting MR ${createdMrIid} to remove source branch fails with ` +
                     `status ${deleteSourceResponse.status} ${deleteSourceResponse.statusText}`);
    }

    prCreatedRegexMatched = true;
}

if (!prCreatedRegexMatched && result.exitCode === 0) {
    console.warn("WARNING: cannot parse git-backporting output for created MR. Bad regex?");
}

/* Post message of what we've done. */
let comment = `\
GitLab auto backporter bot have \
${result.exitCode == 0 ? "successfully backported" : "failed to backport"} \
this MR to the following branch(es): ${bp_targets.join(', ')}
`;

if (result.exitCode !== 0) {
    comment += `\

Output of the backporter follows:

<pre>
${result.output}
</pre>
`;
}

let commentResponse = await fetch(`https://gitlab.com/api/v4/projects/` +
            encodeURIComponent(webhook_payload.project.path_with_namespace) +
            `/merge_requests/${webhook_payload.object_attributes.iid}/notes`,
{
    method: 'POST',
    headers: { 
        "Authorization": `Bearer ${opts.gitlabApiToken}`,
        "Content-Type": "application/json",
    },
    body: JSON.stringify({
        body: comment,
    }),
});

if (!commentResponse.ok) {
    console.warn(`WARNING: posting comment fails with ` +
                 `status ${commentResponse.status} ${commentResponse.statusText}`);
}
