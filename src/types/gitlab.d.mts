/* Types copied from: https://github.com/excaliburhan/node-gitlab-webhook/blob/master/index.d.ts
 * Licensed under MIT license.
 */

export interface LastCommit {
    id: string;
    message: string;
    timestamp: string;
    url: string;
    author: Author;
}

export interface Author {
    name: string;
    email: string;
}

export interface Project {
    name: string;
    description: string;
    web_url: string;
    avatar_url: string;
    git_ssh_url: string;
    git_http_url: string;
    namespace: string;
    visibility_level: number;
    path_with_namespace: string;
    default_branch: string;
    homepage: string;
    url: string;
    ssh_url: string;
    http_url: string;
    id?: number;
}

export interface Repository {
    name: string;
    url: string;
    description: string;
    homepage: string;
}

export interface Label {
    id: number;
    title: string;
    color: string;
    project_id: number;
    created_at: string;
    updated_at: string;
    template: boolean;
    description: string;
    type: string;
    group_id: number;
}

export interface Changes {
    updated_by_id?: number[];
    updated_at?: string[];
    labels?: Labels;
}

export interface Labels {
    previous: Label[];
    current: Label[];
}

export interface User {
    name: string;
    username: string;
    avatar_url: string;
}

export interface MergeRequestEvent {
    object_kind: 'merge_request';
    user: User;
    project: Project;
    repository: Repository;
    object_attributes: MergeRequestAttributes;
    labels: Label[];
    changes: Changes;
}

export interface MergeRequestAttributes {
    id: number;
    target_branch: string;
    source_branch: string;
    source_project_id: number;
    author_id: number;
    assignee_id: number;
    title: string;
    created_at: string;
    updated_at: string;
    milestone_id: null;
    state: string;
    merge_status: string;
    target_project_id: number;
    iid: number;
    description: string;
    source: Project;
    target: Project;
    last_commit: LastCommit;
    work_in_progress: boolean;
    url: string;
    action: string;
    assignee: User;
}
