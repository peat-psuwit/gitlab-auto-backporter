# Allow using GitLab as a proxy to avoid Docker Hub limits.
# https://docs.gitlab.com/ee/user/packages/dependency_proxy/
# https://www.jeffgeerling.com/blog/2017/use-arg-dockerfile-dynamic-image-specification
ARG IMAGE_ORIGIN=docker.io

# Build stage
FROM $IMAGE_ORIGIN/node:20-alpine AS builder

WORKDIR /opt/gitlab-auto-backporter

COPY package*.json ./
RUN npm ci

COPY ./ ./
RUN npm run build && chmod +x ./dist/index.mjs

# Install stage
FROM $IMAGE_ORIGIN/node:20-alpine

# Peer dependency.
RUN apk add --update-cache git && \
    rm -rf /var/cache/apk/*

WORKDIR /opt/gitlab-auto-backporter

COPY package*.json ./
RUN npm ci --omit=dev

COPY --from=builder /opt/gitlab-auto-backporter/dist/ ./dist/
RUN ln -s /opt/gitlab-auto-backporter/dist/index.mjs /usr/local/bin/gitlab-auto-backporter

ENTRYPOINT gitlab-auto-backporter
