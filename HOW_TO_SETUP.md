Set up the bot in your own project(s)
=====================================

This project uses GitLab CI as both the image builder and as the receiver of the webhook in order to avoid additional infrastructure. As such, there's a bit of back and forth in setting up the project.

1. Fork this repository under your own namespace.
2. On the gitlab-auto-backporter project:
    - Go to **Settings > CI/CD**.
    - Expand **Pipeline trigger tokens**, then select **Add new token**.
    - Enter a description and select **Create pipeline trigger token**. Copy the token, and also.
3. On the target project or group:
    - Go to **Settings > Webhooks**, then select **Add new webhook**.
    - For URL, specify: `https://gitlab.com/api/v4/projects/<bot project ID>/ref/main/trigger/pipeline?token=<token>`
        - The bot's project ID can be retrieved from the project's overview page, from the usage examples in **Pipeline trigger tokens** page, or by percent-encoding the project's namespace and name. Use gitlab-auto-backporter's ID, not the target project's.
    - Input anything as **secret**. GitLab's pipeline trigger doesn't read it.
    - Leave **Enable SSL verification** checked, then click **Add webhook**.
    - Then, go to **Settings > Access Tokens**, and select **Add new token**.
        - On GitLab SaaS, this feature may require Premium subscription or higher. In that case, the alternative is to use Personal Access Token instead, but your name will be shown every time the bot works.
    - Specify a name. This will be shown as the user submitting the MR and comment.
    - Set the expiration date to the maximum. For scope, select `api`, `read_repository` and `write_repository`.
    - Click **Create group access token**. Copy the token.
4. Go back to the gitlab-auto-backporter project.
    - Go to **Settings > CI/CD**. Select **Variables**.
    - Select **Add variable**. Set the key to `GITLAB_API_TOKEN`, and the value to the access token.
    - You can also set `GITLAB_AUTO_BACKPORTER_GITUSER` and `GITLAB_AUTO_BACKPORTER_GITEMAIL` to control the committer value of the cherry-picked commits.
5. For each backport target you want to backport to, create a label called `Backport to: <branch name>`. Make sure to have a space after `:`. At the moment the prefix is not configurable yet.
