GitLab auto backporter
======================

Takes GitLab's merge request webhooks, check labels to determine backport target(s), and call [@kie/git-backporting] for actual backport.

[@kie/git-backporting]: https://github.com/kiegroup/git-backporting

How to use
----------

1. On a merge request, add `Backport to: <branch name>` (e.g. ~"Backport to: ubports/focal") to mark the MR as applicable for backporting.
2. When the MR is merged (or immediately if the MR is already merged), the bot will automatically backport the MR to a new branch, and also automatically create an MR targetting the target branch.
3. The bot will report the success (or failure) of the operation via a comment.

How it works
------------

When a merge request is changed in any way, GitLab will send a webhook to trigger this project's CI pipeline. It will read the webhook content, and if it's a merged event or label change event, it will determine the backport targets. It then calls `git-backporting` to do the actual backport and backported MR creation. Finally, it will post the result as a comment to the original MR via the API.

Set up the bot in your own project(s)
-------------------------------------

It's possible to use this bot in your own project or groups. See [HOW_TO_SETUP.MD](./HOW_TO_SETUP.md).

